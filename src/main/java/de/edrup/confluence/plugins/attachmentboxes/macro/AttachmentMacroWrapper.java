package de.edrup.confluence.plugins.attachmentboxes.macro;

import java.util.Map;

import javax.inject.Inject;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;

import de.edrup.confluence.plugins.attachmentboxes.util.AttachmentBoxesHelper;

public class AttachmentMacroWrapper implements Macro {
	
	private final AttachmentBoxesHelper attachmentBoxesHelper;
	private final I18nResolver i18n;
	
	
	@Inject
	public AttachmentMacroWrapper(AttachmentBoxesHelper attachmentBoxesHelper, @ComponentImport I18nResolver i18n) {
		this.attachmentBoxesHelper = attachmentBoxesHelper;
		this.i18n = i18n;
	}

	
	@Override
	public String execute(Map<String, String> parameters, String bodyContent, ConversionContext conversionContext) {
		
		if(!attachmentBoxesHelper.isLicensed()) {
			return i18n.getText("de.edrup.confluence.plugins.attachment-boxes.license");
		}
		
		String labelsToAdd = parameters.containsKey("labelsToAdd") ? parameters.get("labelsToAdd") : "";
		String minimizeBox = parameters.containsKey("minimizeBox") ? parameters.get("minimizeBox") : "false";
		String dropOldLabels = parameters.containsKey("dropOldLabels") ? parameters.get("dropOldLabels") : "true";
		
		String style = "";
		if(parameters.containsKey("borderColor") || parameters.containsKey("bgColor")) {
			style = "border-radius:5px;padding:10px;";
		}
		if(parameters.containsKey("borderColor") && !parameters.get("borderColor").isEmpty()) {
			style = style + "border:2px solid " + parameters.get("borderColor") + ";";
		}
		if(parameters.containsKey("bgColor") && !parameters.get("bgColor").isEmpty()) {
			style = style + "background-color: " + parameters.get("bgColor");
		}
		
		return String.format("<div class='attachment-boxes' labels-to-add='%s' style='%s' minimize-box='%s' drop-old-labels='%s'>%s</div>", sanitize(labelsToAdd), sanitize(style), sanitize(minimizeBox), sanitize(dropOldLabels), bodyContent);
	}

	
	@Override
	public BodyType getBodyType() {
		return BodyType.RICH_TEXT;
	}

	
	@Override
	public OutputType getOutputType() {
		return OutputType.BLOCK;
	}
	
	
	private String sanitize(String value) {
		return value.replaceAll("[&<>'\"]", "");
	}
}
