package de.edrup.confluence.plugins.attachmentboxes.listener;

import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.confluence.event.events.content.attachment.AttachmentCreateEvent;
import com.atlassian.confluence.event.events.content.attachment.AttachmentUpdateEvent;
import com.atlassian.event.api.EventListener;

import de.edrup.confluence.plugins.attachmentboxes.util.AttachmentBoxesHelper;

@Named
public class AttachmentBoxesListener {
	
	private final AttachmentBoxesHelper attachmentBoxesHelper;
	
	
	@Inject
	public AttachmentBoxesListener(AttachmentBoxesHelper attachmentBoxesHelper) {
		this.attachmentBoxesHelper = attachmentBoxesHelper;
	}
	
	
	@EventListener
	public void onAttachmentCreateEvent(AttachmentCreateEvent event) {
		attachmentBoxesHelper.handleAttachmentUploadEvent(event.getAttachment().getContainer().getId(), event.getAttachment().getId());
	}
	
	@EventListener
	public void onAttachmentUpdateEvent(AttachmentUpdateEvent event) {
		attachmentBoxesHelper.handleAttachmentUploadEvent(event.getAttachment().getContainer().getId(), event.getAttachment().getId());
	}
}
