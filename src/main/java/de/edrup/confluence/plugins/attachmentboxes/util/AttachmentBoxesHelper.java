package de.edrup.confluence.plugins.attachmentboxes.util;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.Labelable;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.upm.api.license.PluginLicenseManager;


@Named
public class AttachmentBoxesHelper {
	
	private final AttachmentManager attachmentMan;
	private final PageManager pageMan;
	private final LabelManager labelMan;
	private final TransactionTemplate transactionTemplate;
	private final PluginLicenseManager licenseMan;
	private final Cache<String, String> potentialLabelActionCache;
	private final PermissionManager permissionMan;
	
	private static ExecutorService pool = Executors.newFixedThreadPool(4);
	private static final Logger log = LoggerFactory.getLogger(AttachmentBoxesHelper.class);
	
	
	@Inject
	public AttachmentBoxesHelper(@ComponentImport CacheManager cacheManager, @ComponentImport AttachmentManager attachmentMan,
		@ComponentImport TransactionTemplate transactionTemplate, @ComponentImport LabelManager labelMan, @ComponentImport PluginLicenseManager licenseMan,
		@ComponentImport PageManager pageMan, @ComponentImport PermissionManager permissionMan) {
		potentialLabelActionCache = cacheManager.getCache("Attachment Boxes action cache", null, new CacheSettingsBuilder().expireAfterAccess(5, TimeUnit.MINUTES).build()); 
		this.attachmentMan = attachmentMan;
		this.transactionTemplate = transactionTemplate;
		this.labelMan = labelMan;
		this.licenseMan = licenseMan;
		this.pageMan = pageMan;
		this.permissionMan = permissionMan;
	}
	
	
	public void addToCache(Long pageId, String userKey, String fileNameHash, String labelsToAdd, Long uploadToPage, Boolean dropOldLabels) {
		List<String> hashes = Arrays.asList(fileNameHash.split(","));
		for(String hash : hashes) {
			log.debug("Adding the following potential label action to the cache: {}, {}, {}, {}, {}, {}", pageId, userKey, hash, labelsToAdd, uploadToPage, dropOldLabels);
			potentialLabelActionCache.put(createCacheKey(pageId, userKey, hash), new JSONObject().put("labelsToAdd", labelsToAdd).put("uploadToPage", uploadToPage).put("dropOldLabels", dropOldLabels).toString());
		}
	}
	
	
	private String createCacheKey(Long pageId, String userKey, String hash) {
		return pageId.toString() + ":" + userKey + ":" + hash;
	}
	
		
	public void handleAttachmentUploadEvent(Long pageId, Long attachmentId) {
		transactionTemplate.execute(() -> handleAttachmentUploadEventInTransaction(pageId, attachmentId));
	}
	
	
	private boolean handleAttachmentUploadEventInTransaction(Long pageId, Long attachmentId) {
		Attachment attachment = attachmentMan.getAttachment(attachmentId);
		String cacheKey = createCacheKey(pageId, attachment.getLastModifier().getKey().toString(), Integer.toString(attachment.getTitle().hashCode()));
		String potentialLabelAction = potentialLabelActionCache.get(cacheKey);
		log.debug("Handling attachment upload event: {}, {}, {}", pageId, attachmentId, attachment.getLastModifier().getKey().toString());
		
		if(potentialLabelAction != null) {
			log.debug("Label action found in cache: {}, {}, {}", pageId, attachmentId, potentialLabelAction);
			JSONObject potentialLabelActionJSON = new JSONObject(potentialLabelAction);
						
			if(potentialLabelActionJSON.has("dropOldLabels") && potentialLabelActionJSON.getBoolean("dropOldLabels")) {
				log.debug("Removing all previous labels for attachment with id {}", attachmentId);
				labelMan.removeAllLabels((Labelable) attachment);
			}
			
			String labelsToAdd[] = potentialLabelActionJSON.getString("labelsToAdd").split(" *, *");
			for(String labelToAdd : labelsToAdd) {
				if(!labelToAdd.isEmpty()) {
					log.debug("Adding label {} to attachment with id {}", labelToAdd, attachmentId);
					labelMan.addLabel((Labelable) attachment, new Label(labelToAdd));
				}
			}
			
			if(!pageId.equals(potentialLabelActionJSON.getLong("uploadToPage"))) {
				CompletableFuture.supplyAsync(() -> delayedMove(attachmentId, potentialLabelActionJSON.getLong("uploadToPage")), pool);
			}
			
			potentialLabelActionCache.remove(cacheKey);
			return true;
		}
		else {
			return false;
		}
	}
	
	
	private boolean delayedMove(Long attachmentId, Long toPageId) {
		try {
			Thread.sleep(200);
			log.debug("Moving attachment {} to page {}", attachmentId, toPageId);
			transactionTemplate.execute(() -> moveAttachment(attachmentId, toPageId));
			return true;
		}
		catch(Exception e) {
			log.error(e.toString());
			return false;
		}
	}
	
	
	private boolean moveAttachment(Long attachmentId, Long toPageId) {
		attachmentMan.moveAttachment(attachmentMan.getAttachment(attachmentId), attachmentMan.getAttachment(attachmentId).getFileName(), pageMan.getAbstractPage(toPageId));
		return true;
	}
	
	
	public void moveBox(Long pageId, String labelsToAdd, Long uploadToPage, String uriList) {
		transactionTemplate.execute(() -> moveBoxInTransaction(pageId, labelsToAdd, uploadToPage, uriList));
	}
	
	
	private boolean moveBoxInTransaction(Long pageId, String labelsToAddList, Long uploadToPage, String url) {
		ConfluenceUser user = AuthenticatedUserThreadLocal.get();
		if(permissionMan.hasPermission(user, Permission.EDIT, pageMan.getAbstractPage(pageId)) && permissionMan.hasPermission(user, Permission.EDIT, pageMan.getAbstractPage(uploadToPage))) {
			Attachment attachment = attachmentMan.findAttachmentForDownloadPath(url).get();
			if(attachment != null) {
				labelMan.removeAllLabels((Labelable) attachment);
				String labelsToAdd[] = labelsToAddList.split(" *, *");
				for(String labelToAdd : labelsToAdd) {
					if(!labelToAdd.isEmpty()) {
						log.debug("Adding label {} to attachment with id {}", labelToAdd, attachment.getId());
						labelMan.addLabel((Labelable) attachment, new Label(labelToAdd));
					}
				}
				if(pageId != uploadToPage) {
					log.debug("Moving attachment {} to page {}", attachment.getId(), uploadToPage);
					attachmentMan.moveAttachment(attachment, attachment.getFileName(), pageMan.getAbstractPage(uploadToPage));
				}
				return true;
			}
		}
		return false;
	}
	
	
    public boolean isLicensed() {
    	return licenseMan.getLicense().isDefined() ? licenseMan.getLicense().get().isValid() : false;
    }
}
