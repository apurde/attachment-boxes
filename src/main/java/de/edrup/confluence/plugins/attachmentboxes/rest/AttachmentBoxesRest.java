package de.edrup.confluence.plugins.attachmentboxes.rest;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import de.edrup.confluence.plugins.attachmentboxes.util.AttachmentBoxesHelper;

@Path("/")
public class AttachmentBoxesRest {
	
	private final AttachmentBoxesHelper attachmentBoxesHelper;
	
	
	@Inject
	public AttachmentBoxesRest(AttachmentBoxesHelper attachmentBoxesHelper) {
		this.attachmentBoxesHelper = attachmentBoxesHelper;
	}
	
	
	@PUT
    @Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.TEXT_PLAIN})
	@Path("/registerpotentialupload")
    public Response registerPotentialUpload(String dataAsString) {
		JSONObject data = new JSONObject(dataAsString);
		attachmentBoxesHelper.addToCache(data.getLong("pageId"), data.getString("userKey"), data.getString("fileNameHash"), data.getString("labelsToAdd"), data.getLong("uploadToPage"), data.getBoolean("dropOldLabels"));
		return Response.ok("").build();
    }
	
	
	@PUT
    @Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@Path("/movebox")
    public Response moveBox(@QueryParam("pageId") Long pageId, @QueryParam("labelsToAdd") String labelsToAdd, @QueryParam("url") String url, @QueryParam("uploadToPage") Long uploadToPage) {
		attachmentBoxesHelper.moveBox(pageId, labelsToAdd, uploadToPage, url);
		return Response.ok("").build();
    }
}
