function AttachmentBoxes() {

	var dropFiles = [];
	
	this.init = function() {
		AJS.$(".attachment-boxes").each(function() {
			handleBox(this);
		});
		startObserver();
	};
	
	
	// handle a single box
	var handleBox = function(box) {
		var labelsToAdd = AJS.$(box).attr("labels-to-add");
		if(labelsToAdd == "") {
			labelsToAdd = AJS.$(box).find('[name*="labels"]:first').attr("value") || "";
			// solve issue with the Attachments macro which sets the labels to be added to $macroParams.labels in case no label is defined
			if(labelsToAdd.startsWith("$macroParams")) {
				labelsToAdd = "";
			}
		}
		
		var uploadToPage = AJS.$(box).find(".plugin_attachments_macro_render_param:first").attr("value");
		var dropOldLabels = AJS.$(box).attr("drop-old-labels");

		// bind event handlers

		AJS.$(box).find("input[name='file_0']").on("change", function() {
			var fileName = AJS.$(box).val().replace(/.*(\/|\\)/, '');
			console.log(fileName);
			registerPotentialUpload(hashCode(fileName), labelsToAdd, uploadToPage, dropOldLabels, null);
		});
		
		AJS.$(box).on("drop", function(event) {
			var url = event.originalEvent.dataTransfer.getData("URL");
			if(isEmpty(url)) {
				console.log("Upload via drag and drop detected - posting this and terminating event propagation");
				dropFiles = event.originalEvent.dataTransfer.files;
				registerPotentialUpload(createHashString(dropFiles), labelsToAdd, uploadToPage, dropOldLabels, function() {
					console.log("Posting completed - sending fake drop");
					var bodyElm = AJS.$("body")[0];
					var fakeDropEvent = createNewDragEvent("drop");
					Object.defineProperty(fakeDropEvent, "dataTransfer", {
						value: {
  							files: dropFiles
						}
					});
					bodyElm.dispatchEvent(fakeDropEvent);
				});
				return false;
			}
			else {
				console.log("Move via drag and drop detected - posting this");
				postMove(labelsToAdd, uploadToPage, url);
			}
			
			return true;
		});
		
		AJS.$(box).on("dragenter", function(event) {
			event.preventDefault();
		});
		
		AJS.$(box).on("dragover", function(event) {
			event.preventDefault();
		});

		// change table color if needed
		var bgColor = AJS.$(box).css("background-color");
		if(bgColor.length > 0) {
			AJS.$(box).find("tr").css("background-color", bgColor);
		}
		
		// minimize footprint of Attachments macro if needed
		if(AJS.$(box).attr("minimize-box") == "true") {
			AJS.$(box).find(".attachments-table-drop-zone").css("padding", "5px"); 
			AJS.$(box).find("table.attachments").find("thead").hide();
			AJS.$(box).find("table.attachments").find("tr").find("td").hide();
			AJS.$(box).find("table.attachments").find("tr").find("td:eq(0),td:eq(1)").show();
			AJS.$(box).find("table.attachments").find("tr").find("td:eq(1)").css("width", "100%");
			AJS.$(box).find(".download-all-link,.drop-zone-empty-text,.attachment-comment").hide();
		}
	};
	
	
	// observe for attachment boxes added later
	var startObserver = function() {
		const observer = new MutationObserver(function(mutations) {
			mutations.forEach(function(mutation) {
				mutation.addedNodes.forEach(function(node) {
					// If the added node itself is an attachment-box
					if (node.classList && node.classList.contains('attachment-boxes')) {
						handleBox(node);
					}

					// If the added node contains attachment-box elements within it
					const attachmentBoxes = node.querySelectorAll && node.querySelectorAll('.attachment-boxes');
					if (attachmentBoxes.length) {
						attachmentBoxes.forEach(function(attachmentBox) {
							handleBox(attachmentBox);
						});
					}
				});
			});
		});

		// Start observing the document body or any specific container
		observer.observe(document.body, { childList: true, subtree: true });
	};

	
	// register a potential upload action
	var registerPotentialUpload = function(fileNameHash, labelsToAdd, uploadToPage, dropOldLabels, callback) {
		
		var data = {
			pageId:  AJS.Meta.get("page-id"),
			userKey: AJS.Meta.get("remote-user-key"),
			labelsToAdd: labelsToAdd,
			uploadToPage: uploadToPage,
			fileNameHash: fileNameHash,
			dropOldLabels: dropOldLabels
		};
		
		AJS.$.ajax({
			url: AJS.contextPath() + "/rest/attachmentboxes/1.0/registerpotentialupload",
			type: "PUT",
			dataType: 'json',
            contentType: 'text/plain',
			data: JSON.stringify(data),
			success: function(result) {
				if(callback != null) {
					callback();
				}
			},
			error: function(request, status, error) {
				if(callback != null) {
					callback();
				}
			}
		});
	};
	
	
	// post a move action
	var postMove = function(labelsToAdd, uploadToPage, url) {
		AJS.$.ajax({
			url: AJS.contextPath() + "/rest/attachmentboxes/1.0/movebox?pageId=" + AJS.Meta.get("page-id") +
				"&labelsToAdd=" + labelsToAdd +
				"&uploadToPage=" + uploadToPage +
				"&url=" + url,
			type: "PUT",
			success: function(result) {
				location.reload();
			}
		});		
	};
	
	
	// check if undefined, null or empty
	var isEmpty = function(val){
	    return (val === undefined || val == null || val.length <= 0) ? true : false;
	};
	
	
	// create a (Drag)Event
	var createNewDragEvent = function(eventName) {
	    var event;
	    if (typeof(DragEevent) === 'function') {
	        event = new DragEvent(eventName);
	    } else {
	        event = document.createEvent("Event");
	        event.initEvent(eventName, true, true);
	    }
	    return event;
	};	
	
	
	// create a joined hash string for the dropped files
	var createHashString = function(dropFiles) {
		var hashString = "";
		for(var n = 0; n < dropFiles.length; n++) {
			hashString = hashString + hashCode(dropFiles[n].name) + ",";
		}
		
		return hashString.replace(/,$/, "");
	};
	
	
	// calculate a hash value from the string
	var hashCode = function(s) {
	  var h = 0, l = s.length, i = 0;
	  if ( l > 0 )
	    while (i < l)
	      h = (h << 5) - h + s.charCodeAt(i++) | 0;
	  return h;
	};
}


var attachmentBoxes = new AttachmentBoxes();

AJS.toInit(function() {
	attachmentBoxes.init();
});